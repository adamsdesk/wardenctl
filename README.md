# What Is Wardenctl?

Simply put, Wardenctl (Warden Control) is a Minecraft server CLI (command-line interface) management tool.

# Features

- Automate installation of PaperMC, SpigotMC and Vanilla Minecraft server software.
- Automate Minecraft server software updates/upgrades.
- Automate Minecraft server backups with optional in-game warning notice count down.
- Automate Minecraft [TheNewEconomy](https://github.com/TheNewEconomy/TNE-Bukkit) plugin to extract all player
currency balances with optional data retention cleanup.
- Simplified logs viewer with optional live viewer.
- Access Minecraft server console.
- Ctrlcenter, view Minecraft server logs and console at the same time.
- Automate file/directory permissions and group/ownership.
- Plugin management tool.
  - List installed plugin(s) along with associated information (name, version, dependencies, source website, source version).
  - Manage plugin source websites, add, edit and remove.
- Start Minecraft server.
- Stop/shutdown Minecraft server with optional in-game warning notice count down.
- Status monitoring of services.
  - Minecraft server
  - Minecraft backup
  - Minecraft DDNS (dynamic domain name server)
  - [MinecraftStats](https://github.com/pdinklag/MinecraftStats)
  - Cert (e.g. Let's Encrypt certbot) (SSL/TLS certificate)
  - HTTP (e.g. Nginx, Apache HTTPD)
- Status monitoring of general Minecraft server information
  - Minecraft server software version
  - Minecraft server software version state (how far behind)
  - Minecraft server total users online out of maximum users permitted
  - Minecraft server TPS (1m, 5m, 15m)
- Status monitoring of server operating system
  - Hostname
  - CPU Usage
  - Operating system distribution name
  - Operating system release model
  - Operating system architecture
  - Operating system kernel name, version and build
  - RAM total, used, free and available

# System Requirements

- GNU Linux operating System
- GNU BASH v5.0+
- [minecraft-systemd-services](https://gitlab.com/adouglas/minecraft-systemd-services)
- Operating system packages in ArchLinux
  - awk
  - coreutils (cat, chmod, chown, cut, head, ls, mkdir, mv, rm, tail, uname)
  - cURL
  - findutils (find)
  - git
  - glibc (iconv)
  - grep
  - inetutils (hostname)
  - jdk8-openjdk (jar, java)
  - lsb-release
  - [mcrcon](https://github.com/Tiiffi/mcrcon)
  - procps-ng (free)
  - [pup-git](https://github.com/EricChiang/pup)
  - tar
    - Optional: bzip2, gzip, xz, zstd
  - tmux
- [Systemd](https://freedesktop.org/wiki/Software/systemd/) (journalctl,
systemctl)

# Installation

**Assumptions**

- Have an understanding of general server practices.
- Have experienced working knowledge within a CLI (command-line interface).
- Using a Linux operating system.
- Installed all required dependencies as stated in [System Requirements](#system-requirements) section.
- Dependencies are accessible via [environment PATH](https://wiki.archlinux.org/index.php/Environment_variables).
- Installation is done via Linux CLI.
- Steps prefixed with a "$" (dollar sign) represents the CLI prompt. The text
after the "$" is to be entered at the CLI.
- Steps prefixed with a "#" (number sign) represents the CLI prompt with
elevated user permissions (e.g. root). The text after the "#" is to be entered
at the CLI.
- A single backslash character beside another character is used to escape the
proceeding character. In this case backslash character is not to to be entered
at the CLI.
- These instructions are an example of installation and configuration.

## Manual

1. Download the project.
   ```console
   $ git clone https://gitlab.com/adouglas/wardenctl.git
   ```
1. Change to project directory.
   ```console
   $ cd wardenctl
   ```
1. Copy the files within the
[environment PATH](https://wiki.archlinux.org/index.php/Environment_variables).
    ```console
    # cp wardenctl wardenctl-* /usr/local/bin/
    ```
    ```
    wardenctl
    wardenctl-backup
    wardenctl-common
    wardenctl-install
    wardenctl-plugins
    wardenctl-shutdown
    wardenctl-srvinfo
    wardenctl-statuses
    wardenctl-system
    ```
1. Create configuration directory.
   ```console
   # mkdir /etc/wardenctl
   ```
1. Copy configuration files.
   ```console
   # cp wardenctl.conf pluginsites.conf /etc/wardenctl/
   ```
   ```
   wardenctl.conf
   pluginsites.conf
   ```
1. Set file permissions.
   ```console
   # chmod 755 /usr/local/bin/wardenctl
   # chmod 644 /usr/local/bin/wardenctl-*
   # chmod 644 /etc/wardenctl/*.conf
   ```
   ```
   -rwxr-xr-x /usr/local/bin/wardenctl
   -rw-r--r-- /usr/local/bin/wardenctl-backup
   -rw-r--r-- /usr/local/bin/wardenctl-common
   -rw-r--r-- /usr/local/bin/wardenctl.conf
   -rw-r--r-- /usr/local/bin/wardenctl-install
   -rw-r--r-- /usr/local/bin/wardenctl-plugins
   -rw-r--r-- /usr/local/bin/wardenctl-shutdown
   -rw-r--r-- /usr/local/bin/wardenctl-srvinfo
   -rw-r--r-- /usr/local/bin/wardenctl-statuses
   -rw-r--r-- /usr/local/bin/wardenctl-system
   -rw-r--r-- /etc/wardenctl/pluginsites.conf
   -rw-r--r-- /etc/wardenctl/wardenctl.conf
   ```
1. Set file ownership and group.
   ```console
   # chown root:root /usr/local/bin/wardenctl*
   # chown root:root /etc/wardenctl/*.conf
   ```
   ```
   -rwxr-xr-x 1 root root 9.7K Feb 20 18:53 /usr/local/bin/wardenctl*
   -rw-r--r-- 1 root root 3.3K Feb 20 18:53 /usr/local/bin/wardenctl-backup
   -rw-r--r-- 1 root root 2.9K Feb 20 18:53 /usr/local/bin/wardenctl-common
   -rw-r--r-- 1 root root 6.1K Feb 20 18:53 /usr/local/bin/wardenctl-install
   -rw-r--r-- 1 root root 6.7K Feb 20 18:53 /usr/local/bin/wardenctl-plugins
   -rw-r--r-- 1 root root 1.1K Feb 20 18:53 /usr/local/bin/wardenctl-shutdown
   -rw-r--r-- 1 root root 2.8K Feb 20 18:53 /usr/local/bin/wardenctl-srvinfo
   -rw-r--r-- 1 root root 8.3K Feb 20 18:53 /usr/local/bin/wardenctl-statuses
   -rw-r--r-- 1 root root 2.8K Feb 20 18:53 /usr/local/bin/wardenctl-system
   -rw-r--r-- 1 root root 1.5K Feb 10 14:27 pluginsites.conf
   -rw-r--r-- 1 root root 2.2K Feb  7 23:27 wardenctl.conf
   ```
1. Edit configuration as desired.
   ```console
   # nano /etc/wardenctl/wardenctl.conf
   ```
   **Note:** The "systemd_mcuser" and "systemd_mcgroup" refer to the "User"
   and "Group" properties in [minecraft-systemd-services](https://gitlab.com/adouglas/minecraft-systemd-services) unit files.

   **Note:** Refer to the [Configuration Files](#configuration-files) section for further details.

# Usage

At the CLI run "wardenctl help" without double quotes.

# Configuration Files

The configuration files are stored in "/etc/wardenctl".

**Note:** Where the configuration files are stored cannot be changed.

## Pluginsites.conf

This is an array of Minecraft plugin web site sources. The web site sources
(addresses) are used to fetch metadata about each associated plugin installed
on a server.

**Note:** Do not directly edit "pluginsites.conf". Always use the "plugins"
command.

## Wardenctl.conf

### General Settings

| Setting Property               | Description                                |
| ------------------------------ | -------------------------------------------|
| server_host                    | Server host name or IP. address.                                                                        |
| server_type                    | Minecraft server software type (paper, spigot or vanilla).                                             |
| dir_root                       | Minecraft server absolute directory path for root.                         |
| dir_backup                     | Backup absolute directory path for "dir_server".                   |
| dir_server                     | Minecraft server software installation absolute directory path. |
| dir_build                      | Build/temporary absolute directory path for installation, updates/upgrades and plugin management. |
| perm_dir                       | The permission to apply on the directories using "perm" command. See "systemd_mcuser" and "systemd_mcgroup". |
| perm_file                      | The permission to apply on the files using "perm" command. See "systemd_mcuser" and "systemd_mcgroup". |
| tne_extract                    | If TheNewEconomy plugin is installed the extraction of player's balances will be done at backup when enabled (true). A value of false disables this feature. |
| tne_extract_age                | The TheNewEconomy extract file(s) retention age in minutes. If enabled, tne extract file(s) are deleted automatically. A value of 0 (zero) disables this feature. |

### Systemd Settings

| Setting Property               | Description                                |
| ------------------------------ | -------------------------------------------|
| systemd_mcserver               | Minecraft server Systemd service unit name. |
| systemd_mcbackup               | Minecraft backup Systend service unit name. |
| systemd_mcbackup_timer         | Minecraft backup Systemd timer unit name. |
| systemd_ddns                   | Dynamic DNS Systemd service unit name. |
| systemd_ddns_timer             | Dynamic DNS Systemd timer unit name. |
| systemd_mcstats                | MinecraftStats Systemd service unit name. |
| systemd_mcstats_timer          | MinecraftStats Systemd timer unit name. |
| systemd_cert                   | Let's Encrypt Systemd service unit name. |
| systemd_cert_timer             | Let's Encrypt Systemd timer unit name. |
| systemd_http                   | HTTP Systemd service unit name. |
| systemd_mcuser                 | Systemd username to run services as. Also used to set ownership when running "perm" command. See "perm_dir" and "perm_file". |
| systemd_mcgroup                | Systemd group name to run services as. Also used to set ownership when running "perm" command. See "perm_dir" and "perm_file". |

### Minecraft Server Stop Settings

| Setting Property               | Description                                |
| ------------------------------ | -------------------------------------------|
| shutdown                       | The amount of seconds to warn in game of service shutdown. A value of 0 (zero) disables this feature. |
| alert_preffix                  | Prefix value used in front of "shutdown_preffix_msg" setting. |
| shutdown_preffix_msg           | Shutdown prefix message notice shown in game. Used after "alert_preffix" and before "shutdown_suffix_msg". |
| shutdown_suffix_msg            | Shutdown suffix message notice shown in game. Used after "shutdown_preffix_msg". |

### Minecraft Server Backup Settings

| Setting Property               | Description                                |
| ------------------------------ | -------------------------------------------|
| archive_name                   | Backup archive filename (e.g. $(date +%Y%m%d%H%M).tar.zst) ). The archive extension determines archive type (e.g. gzip, tar, xz, zstd). |
| backup_age                     | Backup retention age in minutes. If enabled, backups are deleted automatically. A value of 0 (zero) disables this feature. |
| backup_total_users             | The total amount of users or more must be online before backup will run. A value of 0 (zero) disables this feature. |
| backup_warning1                | How many minutes before first warning is sent in game before backup will commence. Value must be greater than "backup_warning2". A value of 0 (zero) disables this feature. |
| backup_warning2                | How many minutes before second warning is sent in game before backup will commence. Value must be less than "backup_warning1". A value of 0 (zero) disables this feature. |
| backup_warning_prefix_msg      | Backup in game warning prefix message used in front of "backup_warning1_msg" and "backup_warning2_msg". Only used if "backup_warning1" or "backup_warning2" is enabled. |
| backup_warning1_msg            | Backup in game warning message one used after "backup_warning_prefix_msg". Only used if "backup_warning1" is enabled. |
| backup_warning2_msg            | Backup in game warning message two used after "backup_warning_prefix_msg". Only used if "backup_warning2" is enabled. |

# Directory Structure

The directory structure illustrated below is built automatically at installation
based upon the configuration file settings (dir_root, dir_backup, dir_server,
dir_build).

```shell
dir_root="/srv/minecraft/paper"
dir_backup="/srv/minecraft/paper/backup"
dir_server="/srv/minecraft/paper/server"
dir_build="/srv/minecraft/paper/build"
```
```console
/srv/minecraft/
└── paper                   < Minecraft server type (paper, spigot or vanilla).
    ├── backup              < Where all backups are stored.
    │   └── server
    │       ├── archives    < Backup archives.
    │       ├── jar         < Backup of Minecraft jars.
    │       ├── plugins     < Backup of Minecraft plugins (manual).
    │       └── sync        < Backup sync of Minecraft server directory.
    ├── build               < Build/temporary directory used for installation, updates/upgrades and plugin management.
    |   └── pluginstmp      < A temporary directory created/removed automatically for plugin management.
    └── server              < Installation directory of Minecraft server software.
```

**Note:** The "dir_backup" sub-directories are created automatically and
cannot be changed within the configuration file.

**Note:** The "dir_backup/jar" sub-directory is there for manual use to backup
Minecraft plugins.

# License

Wardenctl is licensed under the GNU General Public License v3.0. Review the
license by viewing the LICENSE file.

# Copyright Notice

Copyright (c) 2019-2020 Adam Douglas Some Rights Reserved
