# Changelog
All notable changes to the Wardenctl project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.0] - 2020-02-20
### Added
- Version output (#17, #19).
- Host system information command, "wardenctl system" (#21).
- System command help (#21).
- Documentation for each configuration setting (#25).
- Documentation for directory strucutre and purpose of each directory (#26).
### Changed
- Correct help documentation for the help command.
- Add version documentation to help (#17).
- Message(s) will no longer output a line feed when empty.
- Remove use of echo for system portability and consistency of message output to
the end user (#20).

## [1.4.0] - 2020-02-10
### Added
- Fetching plugin metadata from GitHub releases (#7).
- Viewing of all service's logs under the logs command (#11).
### Changed
- Update plugins.conf source website addresses (#8).
- Update help view with that associated to added and removed sections (#13).
- References to "nginx" has been changed to "http" for clarity and consistency (#16).
### Removed
- Backup logs command. This command is now redundant with the logs command (#12).
- Minecraft world time status. Not useful. (#14).

## [1.3.0] - 2020-02-07
### Added
- New feature in config to set the desired file and directory permissions.

## [1.2.0] - 2020-02-07
### Added
- CONTRIBUTING.md
- Fully implemented Minecraft vanilla server install/update/upgrade.
### Changed
- Add license reference (README.md).
- Minor system requirement revisions (README.md).
- Add missing install step to edit wardenctl.conf (README.md).
- Add steps to create user and group (README.md).
- Add example of copied files (README.md).
- Add description of arguments (READMME.md).
- Change config defaults backup_warning1 and backup_warning2 (wardenctl.conf).
### Fixed
- Correct logic for install to not validate directories (#1).
- Fix install logic to not start server (#2).
- Fix install redundant ownership (#3).
- Allow help command without validating directories.
### Removed
- Remove steps to create user and group (README.md). This is not directly
required for this project.

## [1.1.0] - 2020-02-06
### Added
- CHANGELOG.md
- LICENSE
- README.md
### Removed
- .gitignore

## [1.0.0] - 2020-02-05
### Changed
- Refactored the entire code base to...
  - Clear organization
  - Ease of use
  - Improve performance
  - Reduce code
