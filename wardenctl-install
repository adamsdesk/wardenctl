#!/usr/bin/env bash
installValidateSettings() {
    isUserRoot
    isServerActive
    isBackupActive
    local mcver_regex="^[a-zA-Z0-9_.-]+$"

    if [[ ! "$2" ]]; then
        quit "Error: Parameter not provided. Must provide desired Minecraft server type (paper, spigot or vanilla)."
    fi

    if [[ ! "$3" ]]; then
        quit "Error: Parameter not provided. Must provide desired Minecraft version (e.g. 1.15.0)."
    fi

    if [[ "$2" != 'paper' && "$2" != "spigot" && "$2" != "vanilla" ]]
    then
        quit "Error: Invalid parameter. Second parameter value must be 'paper', 'spigot' or 'vanilla'."
    fi

    if [[ "$2" != "$server_type" ]]; then
        quit "Error: Parameter value of \"$2\" does not equal the configuration setting \"server_type\"."
    fi

    if [[ ! "$3" =~ $mcver_regex ]]; then
        quit "Error: Parameter invalid. Must provide a valid Minecraft version (eg. 1.15.0)."
    fi

    if [[ -z "$4" && "$2" = 'paper' ]]; then
        quit "Error: Parameter not provided. Must provide desired build."
    elif [[ -z "$4" && "$2" = 'spigot' ]]; then
        quit "Error: Parameter not provided. Must provide desired build."
    elif [[ "$4" && "$2" = 'vanilla' ]]; then
        quit "Error: Build parameter was provided. Vanilla does not require a build parameter."
    fi

    if [[ "$4" != 'latest' && "$4" -le 0 && "$2" = 'paper' ]]; then
        quit "Error: Parameter invalid. Must provide a valid PaperMC build."
    elif [[ "$2" = 'spigot' ]]; then
        installValidateSpigotBuild $4
    fi
}

installValidateSpigotBuild() {
    local builds=("lastBuild" "lastStableBuild" "lastSuccessfulBuild" "lastFailedBuild" "lastUnsuccessfulBuild" "lastCompletedBuild")
    local match=0
    local IFS=','

    for build in "${builds[@]}"; do
        if [[ "$build" = "$1" ]]; then
            match=1
            break
        fi
    done

    if [[ "$match" = 0 ]]; then
        echo "Error: Parameter invalid. Must provide a valid SpigotMC build (${builds[*]})."
    fi
}

installCreateDirs() {
    mkdir -p "$dir_root" || quit "Error: Failed to create the directory \"$dir_root\". Opeartion has been halted."
    mkdir -p "$dir_backup" || quit "Error: Failed to create the directory \"$dir_backup\". Opeartion has been halted."
    mkdir -p "$dir_backup/server/plugins" || quit "Error: Failed to create the directory \"$dir_backup/server/plugins\". Opeartion has been halted."
    mkdir -p "$dir_backup/server/jar" || quit "Error: Failed to create the directory \"$dir_backup/server/jar\". Opeartion has been halted."
    mkdir -p "$dir_backup/server/sync" || quit "Error: Failed to create the directory \"$dir_backup/server/sync\". Opeartion has been halted."
    mkdir -p "$dir_backup/server/archives" || quit "Error: Failed to create the directory \"$dir_backup/server/archives\". Opeartion has been halted."
    mkdir -p "$dir_build" || quit "Error: Failed to create the directory \"$dir_build\". Opeartion has been halted."
    mkdir -p "$dir_server" || quit "Error: Failed to create the directory \"$dir_server\". Opeartion has been halted."
}

installSpigot() {
    # Download
    printf "%b\n" "Downloading BuildTools..."
    cd "$dir_build" || quit "Error: Unable to change directory to $dir_build."
    curl -Lo BuildTools.jar "https://hub.spigotmc.org/jenkins/job/BuildTools/$2/artifact/target/BuildTools.jar" || quit "Error: Downloading of SpigotMC has failed. Operation has been halted."
    printf "%b\n" "Download Successful"

    # Build
    printf "%b\n" "Building version $1..."
    java -jar BuildTools.jar --rev "$1" &> lastbuild.log || quit "Error: Build failed. Update operation has been halted."
    printf "%b\n" "Build Successful"

    # Backup jar
    if ls $dir_build/spigot-*.jar 1> /dev/null 2>&1
    then
        printf "%b\n" "Backup SpigotMC jar..."
        cp $dir_build/spigot-*.jar "$dir_backup/server/jar/" || quit "Error: Backup current version has failed. Update operation has been halted."
        printf "%b\n" "Backup Successful"
    fi

    # Install
    printf "%b\n" "Installing SpigotMC..."
    mv spigot-*.jar $dir_server/spigot.jar || quit "Error: Installation has failed. Update operation has been halted."
    printf "%b\n" "Install Successful"
}

installPaper() {
    printf "%b\n" "Downloading PaperMC..."
    cd "$dir_build" || quit "Error: Unable to change directory to $dir_build. Operation has been halted."
    # Download
    #-O uses the remote name.
    #-J forces the -O to get that name from the content-disposition header rather than the URL.
    #-L follows redirects if necessary.
    curl -JLO "https://papermc.io/api/v1/paper/$1/$2/download" || quit "Error: Downloading of PaperMC has failed. Operation has been halted."
    printf "%b\n" "Download Successful"

    # Backup jar
    if ls $dir_build/paper-*.jar 1> /dev/null 2>&1
    then
        printf "%b\n" "Backup PaperMC jar..."
        cp $dir_build/paper-*.jar "$dir_backup/server/jar/"
        printf "%b\n" "Backup Successful"
    fi

    # Install
    printf "%b\n" "Installing PaperMC..."
    mv paper-*.jar $dir_server/paper.jar
    printf "%b\n" "Install successful"
}

installVanilla() {
    printf "%b\n" "Downloading Vanilla..."
    cd "$dir_build" || quit "Error: Unable to change directory to \"$dir_build\". Operation has been halted."
    local url
    local pattern
    pattern="a[download=minecraft_server-$1.jar] attr{href}"
    url="$(curl -s https://mcversions.net/download/$1 | pup $pattern)"
    curl -Lo vanilla-$1.jar $url || quit "Error: Downloading of Vanilla has failed. Operatioin has been halted."
    printf "%b\n" "Download Successful"

    # Backup jar
    if ls $dir_build/vanilla-*.jar 1> /dev/null 2>&1
    then
        printf "%b\n" "Backup Vanilla jar..."
        cp $dir_build/vanilla-*.jar "$dir_backup/server/jar/"
        printf "%b\n" "Backup Successful"
    fi

    # Install
    printf "%b\n" "Installing Vanilla..."
    mv vanilla-*.jar $dir_server/vanilla.jar || quit "Error: Failed to install \"vanilla.jar\" to \"$dir_server\". Operation has been halted."
    printf "%b\n" "Install Successful"
}

installServer() {
    if [[ "$1" = 'paper' ]]; then
        installPaper $2 $3
    elif [[ "$1" = 'spigot' ]]; then
        installSpigot $2 $3
    elif [[ "$1" = 'vanilla' ]]; then
        installVanilla $2
    fi
}

installEula() {
    if [[ ! -e "$dir_server/eula.txt" ]]
    then
        printf "%b\n" "Pausing 30 seconds for processing..."
        #This command is dilberately issued to avoid java errors.
        systemctl start "$systemd_mcserver"
        sleep 30
        #This command is dilberately issued to avoid java errors.
        systemctl stop "$systemd_mcserver"
    fi

    if [[ -e  "$dir_server/eula.txt" && "$(cat $dir_server/eula.txt | tail -n1)" = 'eula=false' ]]
    then
        msgs+=("Warning: You need to agree to the EULA in order to run the server. Edit \"$dir_server/eula.txt\" as described within the file.")
        msgs+=("Notice: Ensure to enable rcon along with setting the rcon password and the rcon port within \"$dir_server/server.properties\".")
    fi
}

install() {
    installValidateSettings $1 $2 $3 $4
    installCreateDirs
    installServer $2 $3 $4
    setPerms
    installEula
    msgs+=("The process ran for $(($SECONDS / 60)) minutes and $(($SECONDS % 60)) seconds.")
}
