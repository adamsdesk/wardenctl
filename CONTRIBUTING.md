# Contributing To Wardenctl

- [Code Of Conduct](#code-of-conduct)
- [Submission Guidelines](#submission-guidelines)
- [Commit Message Guidelines](#commit-message-guidelines)

## Code Of Conduct

As contributors and maintainers of the Wardenctl project, we pledge to respect everyone who contributes by posting issues, updating documentation, submitting
pull requests, providing feedback in comments and any other associated
project activities.

### Principles

- Be constructive
- Be positive
- Be respectful
- Be courtesy
- Be honest
- Be trustworthy
- Never resort to personal attacks
- Never troll
- Never publicly or privately harass
- Never insult

### Violations

If a community member associated to the project violates this code of conduct,
the maintainers of the Wardenctl project may take appropriate actions as
deemed necessary. Such actions maybe but not limited to, removing issues,
comments, public relations and/or blocking accounts.

### Feedback

If you are subject to or witness of unacceptable behavior or have any other
concerns, please email us at feedback@adamsdesk.com.

## Submission Guidelines

### Submitting An Issue

- All issues must be submitted using GitLab issues.
- Search first before creating an issue.
- Preview the issue to ensure it is rendering as intended.
- Be as descriptive as possible.
- Provide steps required to re-produce.
- Utilize the provided form fields.
- Attach necessary reference documents (error messages, screenshots, sample
data, logs, etc.).
- **Do Not** submit or reply to an issue outside of GitLab.
- **Do Not** submit multiple issues within one submission.
- **Do Not** duplicate an issue.

### Commenting On A Issue

- **Do Not** comment on a issue outside of GitLab.
- Stay on topic per the issue description.

### Issue Closure

Valid reasons for closing an issue:

- Duplicate issue.
  -  A reference to the duplicate must be added before closure.
- Lacks enough information to re-produce.
- No longer relevant.
- Off topic.
- Resolved.
- Multiple issues within the submission.
- Will not be resolved.
  - Provide a reason that this issue will not be resolved.

### Issue Re-Opening

- Reasons for re-opening:
  - Issue has not been resolved.
- A detailed comment is required.
- **Do Not** re-open an issue that was closed more than 2 months ago. Instead
Create a new issue.

## Commit Message Guidelines

Git commit messages must adhere to a specific format. This is done to ensure
good communication, consistency and readability.

### Commit Message Format

Each commit message consists of three properties as shown below.

Property | Sub-Property  | Character Limit | Mandatory
---------|---------------|-----------------|----------
header   | type, subject | 100             | True
body     |               | 100             | False
footer   |               | 100             | False

Structure
```
<type>: <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

#### Type

Use one of the following applicable type.

- build
  - Changes that affect the build system or external dependencies.
- ci
  - Changes continuous integration (CI) configuration files and scripts.
- docs
  - Documentation only changes.
- feat
  - A new feature.
- fix
  - A bug fix.
- perf
  - A code change that improves performance.
- refactor
  - A code change that neither fixes a bug nor adds a feature.
- revert
  - When commit reverts a previous commit.
- style
  - Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc.).
- test
  - Adding or modifying tests.

#### Subject

- Be concise description of the change.
- Use the imperative, present tense: "change" not "changed" nor "changes".
- Do not capitalize the first letter.
- No period/dot (.) at the end.

#### Body

- Use the imperative, present tense: "change" not "changed" nor "changes".
- Include the motivation for the change and contrast this with previous
behavior.
- If commit type is **revert**, in the body state `This reverts commit <hash>`.
The `<hash>` is the hash value of the commit being reverted.

#### Footer

- Reference GitLab issue(s) that the commit closes.
- State breaking changes.
  - Breaking changes start with **BREAKING CHANGE:** with a space or two
  newlines. Following is the description of change, motivation for change and migration notes.
