#!/usr/bin/env bash

checkDepdenencies() {
    checkUserExists "$systemd_mcuser"
    checkGroupExists "$systemd_mcgroup"

    for i in "${dependencies[@]}";
    do
        checkCmdExists "$i"
    done

    if [[ $cmdNotFound != '' ]]
    then
        quit "Error: The following command(s) where not found:${cmdNotFound::-1}."
    fi
}

checkCmdExists() {
    if ! [ -x "$(command -v $1)" ]
    then
        cmdNotFound="$cmdNotFound $1,"
    fi
}

getMcServerProperties() {
    if [[ ! -f "$dir_server/server.properties" ]]; then
        quit "Error: The \"$dir_server/server.properties\" doesn't exist and/or is not accessible."
    fi

    # - IFS='' (or IFS=) prevents leading/trailing whitespace from being trimmed.
    # - -r prevents backslash escapes from being interpreted.
    # - || [[ -n $line ]] prevents the last line from being ignored if it doesn't end with a \n (since read returns a non-zero exit code when it encounters EOF).
    while IFS='' read -r line || [[ -n "$line" ]]; do
        # ignore commented lines and empty lines
        [[ "$line" =~ ^#.*$ ]] || [[ "$line" =~ ^$ ]] && continue
        server_properties["${line%=*}"]="${line##*=}"
    done <<< "$(cat "$dir_server/server.properties")"
}

getActiveState() {
    systemd_active_state[mcserver]="$(systemctl is-active $systemd_mcserver)"
    systemd_active_state[mcbackup]="$(systemctl is-active $systemd_mcbackup)"
}

isBackupActive() {
    if [[ "${systemd_active_state[mcbackup]}" = "active" ]]; then
        quit "Error: The backup ($systemd_mcbackup) is currently running, operation has been halted."
    fi
}

isServerActive() {
    if [[ "${systemd_active_state[mcserver]}" = "active" ]]; then
        quit "Error: Minecraft server ($systemd_mcserver) is currently running, operation has been halted."
    fi
}

isUserRoot() {
    if [[ "$(id -u)" -ne 0 ]]
    then
        quit "Error: \"$0 $1\" must be run as root."
    fi
}

checkUserExists() {
    if [[ ! "$(id -u $1)" ]]; then
        quit "Error: The user \"$1\" doesn't exist."
    fi
}

checkGroupExists() {
    if [[ ! "$(id -g $1)" ]]; then
        quit "Error: The group \"$1\" doesn't exist."
    fi
}

setPerms() {
    isUserRoot
    find $dir_server/ -type d -exec chmod "$perm_dir" {} \;
    find $dir_server/ -type f -exec chmod "$perm_file" {} \;
    find $dir_backup/ -type d -exec chmod "$perm_dir" {} \;
    find $dir_backup/ -type f -exec chmod "$perm_file" {} \;
    #required for MinecraftStats.
    if [[ -e "$dir_server/server-icon.png" ]]; then
        chmod 664 "$dir_server/server-icon.png"
    fi
    chown -R ${systemd_mcuser}:${systemd_mcgroup} $dir_server/
    chown -R ${systemd_mcuser}:${systemd_mcgroup} $dir_backup/
}

stopMcServer() {
    isUserRoot
    shutdownServer
}

startMcServer() {
    isUserRoot
    systemctl start "$systemd_mcserver"

    if [[ "$(systemctl is-active $systemd_mcserver)" != 'active' ]]
    then
        quit "Error: Unable to start the Minecraft server service ($systemd_mcserver)."
    fi
}
